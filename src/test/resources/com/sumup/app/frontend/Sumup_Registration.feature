Feature: SumUp app Registration validation

  Scenario: Validation of SumUp Registration process
    Given the user visits the registration page
    When the user fills in email-id and password
    And the user verifies the terms and conditions
    And proceed with registration
    And fill in "user.details"
    And user selects Bankwire as a payment method and proceed further
    Then user can able to see order successful message
