package com.sumup.app.frontend.pages;

import com.sumup.app.frontend.common.data.utils.InputFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OrderPage extends ParentWebPage{

    private final By firstName= By.name("first_name");
    private final By lastName= By.name("last_name");
    private final By address= By.name("address_line1");
    private final By houseNumber= By.name("house_number");
    private final By postCode= By.name("post_code");
    private final By city= By.name("city");
    private final By paymentMethod= By.xpath("//*[@for='payment-method-bankwire']");
    private final By placeOrder= By.cssSelector(".btn.btn--highlight");
    private final By orderSuccessful= By.cssSelector(".h2.h2--dialogue.ng-binding.ng-scope");
    private final WebDriverWait wait = new WebDriverWait(getWebDriver(), 30);

    public void fillAddressDetails(InputFactory userInfo){
        getWebDriver().findElement(firstName).sendKeys(userInfo.getFirstName());
        getWebDriver().findElement(lastName).sendKeys(userInfo.getLastName());
        getWebDriver().findElement(address).sendKeys(userInfo.getAddress());
        getWebDriver().findElement(houseNumber).sendKeys(userInfo.getHouseNumber());
        getWebDriver().findElement(postCode).sendKeys(userInfo.getPostalCode());
        getWebDriver().findElement(city).sendKeys(userInfo.getCity());
    }

    public void selectPaymentMethod(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(paymentMethod));
        getWebDriver().findElement(paymentMethod).click();
    }

    public void placeOrder(){
        getWebDriver().findElements(placeOrder).get(1).click();
    }

    public boolean isOrderSuccessful(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(orderSuccessful));
        return getWebDriver().findElement(orderSuccessful).isDisplayed();
    }
}
