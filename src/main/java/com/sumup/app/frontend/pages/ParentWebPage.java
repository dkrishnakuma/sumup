package com.sumup.app.frontend.pages;

import com.sumup.app.frontend.common.data.InputParameters;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import java.util.concurrent.TimeUnit;

public class ParentWebPage {

    private static WebDriver webDriver;
    private static boolean active = false;
    private static final String registrationPageURL="https://me.sumup.com/de-de/signup/create-account";

    private static String browser;
    private final String DEFAULT_BROWSER = "firefox";

    private InputParameters inputParameters;

    public ParentWebPage() {
        inputParameters = new InputParameters();
    }

    public WebDriver getWebDriver() {
        if (!active) initWebDriver();
        return webDriver;
    }

    public void initWebDriver() {
        parseInputParameters();
        launchBrowser();
    }

    public void launchBrowser(){
        if (!active) {
            if (browser.equalsIgnoreCase("firefox")) {
                System.setProperty("webdriver.gecko.driver", "src/test/resources/selenium-drivers/geckodriver.exe");
                webDriver = new FirefoxDriver();
            } else if (browser.equalsIgnoreCase("chrome")) {
                System.setProperty("webdriver.chrome.driver", "src/test/resources/selenium-drivers/chromedriver.exe");
                webDriver = new ChromeDriver();
            }
            active = true;
            webDriver.manage().window().setSize(new Dimension(1800, 1080));
            webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        }
    }

    public void parseInputParameters(){
        if(System.getProperty("browser")!=null) browser = System.getProperty("browser");
        if (!inputParameters.isValidBrowser(browser)) browser = DEFAULT_BROWSER;
    }

    public String getregistrationPageURL() {
        return registrationPageURL;
    }

    public void setActiveStatus(boolean status) {
        active = status;
    }

    public String getBrowser(){ return browser;}
}
