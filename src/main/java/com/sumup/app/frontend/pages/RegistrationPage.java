package com.sumup.app.frontend.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Timestamp;
import java.util.ArrayList;

public class RegistrationPage extends ParentWebPage {

    private final By emailID = By.cssSelector("#username");
    private final By repeatedEmailID = By.cssSelector("#username_repeat");
    private final By passwordtxt = By.cssSelector("#password");
    private final By proceed = By.cssSelector(".button.ng-binding");
    private final By termsAndConditions = By.cssSelector("p.ng-binding a");
    private final By termsAndConditionsContent = By.cssSelector(".page.main-content.wrapper");
    private ArrayList<String> tabs2;
    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    private long randomNumber = timestamp.getTime();
    private final String EMAILID ="sumup";
    private final String PASSWORD ="12345678";
    private final WebDriverWait wait = new WebDriverWait(getWebDriver(), 30);

    public void launchRegistrationPage() {
        getWebDriver().navigate().to(getregistrationPageURL());
    }

    public boolean istermsAndConditionsContentDisplayed(){
        tabs2 = new ArrayList<String>(getWebDriver().getWindowHandles());
        getWebDriver().switchTo().window(tabs2.get(1));
        return getWebDriver().findElement(termsAndConditionsContent).isDisplayed();
    }

    public void backToParentTab(){
        getWebDriver().close();
        getWebDriver().switchTo().window(tabs2.get(0));
    }

    public void termsAndConditions() {
        getWebDriver().findElement(termsAndConditions).click();
    }

    public void proceedWithRegistration() {
        getWebDriver().findElement(proceed).click();
    }

    public void enterEmailIDAndPassword() {
        String emailAddress = String.format("%s%s@gmail.com",EMAILID,randomNumber);
        wait.until(ExpectedConditions.visibilityOfElementLocated(emailID));
        getWebDriver().findElement(emailID).sendKeys(emailAddress);
        getWebDriver().findElement(repeatedEmailID).sendKeys(emailAddress);
        getWebDriver().findElement(passwordtxt).sendKeys(PASSWORD);
    }
}
