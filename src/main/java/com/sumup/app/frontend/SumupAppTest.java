package com.sumup.app.frontend;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@Cucumber.Options(
        features = "src/test/resources",
        glue = "com.sumup.app.frontend",
        format = { "html:test-report/RunSumupFrontEndTest", "rerun:failed-scenarios.txt" })

public class SumupAppTest {

}