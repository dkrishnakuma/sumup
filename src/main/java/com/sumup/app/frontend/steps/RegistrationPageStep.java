package com.sumup.app.frontend.steps;

import com.sumup.app.frontend.pages.RegistrationPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import org.junit.Assert;


public class RegistrationPageStep {

    private final RegistrationPage registrationPage;

    public RegistrationPageStep(){
        registrationPage = new RegistrationPage();
    }

    @Given("^the user visits the registration page$")
    public void userVisitsRegistrationPage() {
        registrationPage.launchRegistrationPage();
    }

    @And("^the user fills in email-id and password$")
    public void userEnterEmailAndPassword() {
        registrationPage.enterEmailIDAndPassword();
    }

    @And("^the user verifies the terms and conditions$")
    public void userChecksTermsAndConditions() {
        registrationPage.termsAndConditions();
        Boolean terms=registrationPage.istermsAndConditionsContentDisplayed();
        Assert.assertTrue(terms);
        registrationPage.backToParentTab();
    }

    @And("^proceed with registration$")
    public void userProceedWithRegistration() {
        registrationPage.proceedWithRegistration();
    }
}
