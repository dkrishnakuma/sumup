package com.sumup.app.frontend.steps;

import com.sumup.app.frontend.common.data.utils.InputFactory;
import com.sumup.app.frontend.pages.OrderPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.json.simple.parser.ParseException;
import org.junit.Assert;

import java.io.IOException;

public class OrderPageStep {
    private final OrderPage orderPage;

    public OrderPageStep(){
        orderPage = new OrderPage();
    }

    @And("^fill in \"(.*?)\"$")
    public void userFillAddressDetails(String testDataBundle) throws IOException, ParseException {
        InputFactory userInfo = InputFactory.generateUserDataFromConfig(testDataBundle);
        orderPage.fillAddressDetails(userInfo);
    }

    @And("^user selects Bankwire as a payment method and proceed further$")
    public void userSelectPaymentMethod() {
        orderPage.selectPaymentMethod();
        orderPage.placeOrder();
    }

    @Then("^user can able to see order successful message$")
    public void OrderSuccessfulMessage() {
        Assert.assertTrue(orderPage.isOrderSuccessful());
    }
}
