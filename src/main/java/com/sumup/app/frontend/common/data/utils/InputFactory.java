package com.sumup.app.frontend.common.data.utils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class InputFactory {
    private final static String TEST_DATA_CONFIG = "src/test/resources/testdata.json";

    private final String firstName;
    private final String lastName;
    private final String address;
    private final String houseNumber;
    private final String postalCode;
    private final String city;

    public InputFactory(String firstName, String lastName, String address, String houseNumber, String postalCode, String city) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.houseNumber = houseNumber;
        this.postalCode = postalCode;
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    public static InputFactory generateUserDataFromConfig(String testBundle) throws IOException, ParseException {
        FileReader reader = new FileReader(TEST_DATA_CONFIG);
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);

        String firstName = ((JSONObject) jsonObject.get(testBundle)).get("firstName").toString();
        String lastName = ((JSONObject) jsonObject.get(testBundle)).get("lastName").toString();
        String address = ((JSONObject) jsonObject.get(testBundle)).get("address").toString();
        String houseNumber = ((JSONObject) jsonObject.get(testBundle)).get("houseNumber").toString();
        String postalCode = ((JSONObject) jsonObject.get(testBundle)).get("postalCode").toString();
        String city = ((JSONObject) jsonObject.get(testBundle)).get("city").toString();
        return new InputFactory(firstName, lastName,address,houseNumber,postalCode,city);
    }
}

