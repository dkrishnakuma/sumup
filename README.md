# Overview:

The tests will confirm the following,

 1. The test starts at launching https://me.sumup.com/de-de/signup/create-account
 2. Fill in e-mail and password data
 3. Verify that Terms & Conditions (AGB) link is on page
 4. Proceed with registration
 5. On next step fill in user data (first, last names, address)
 6. Select Bankwire transfer (Vorkasse) for payment method
 7. Proceed with next step (Jetzt kaufen)

### Tech Stack
- Selenium Webdriver 
- Cucumber 
- Java 8
- Maven

### Pre-requisites
- Apache Maven 3.0.5 or above
- Java version: 1.8.0_65
- Install Apache Maven and Java 1.8.0.
- Clone/download the project to the folder of your choice.
- Cd into the root folder
- Run the tests using maven - `mvn clean test -Dbrowser=browername`
    - Browser Name can be firefox and chrome

### Features
- Cross Browser testing: Test will run on the below browsers.
    - Chrome
    - Firefox
- Reports: Html reports with screen-shots(for failed cases) will be available under test-report folder